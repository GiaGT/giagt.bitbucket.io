var app = function() {

    $(function() {
        initialize();
        toggleSettings();
        switchTheme();
        navToggleRight();
        navToggleLeft();
        navToggleSub();
        profileToggle();
        switcheryToggle();
        widgetToggle();
        widgetClose();
        widgetFlip();
        tooltips();
        fullscreenWidget();
        fullscreenMode();
        router.initialize();
    });

    var systemStatus = 1;
    var serverAddress = "";
    var mySecret = "";
    var myAddress = "";
    var myPubKey = "";
    var destinationAddress = "";
    var xrpl;
    var fee;
    var balance = 0;
    var xrpReserve = 0;
    var lastTX = 0;
    var connectionStatus = 0;
    var fileHash = "";
    var fileName = "";
    var encryptedHex;
    var messageToSend;
    var txID = "";
    var searchVal;
    // Testing message password: 981gydfrqtsg6xm8m1l8959n34ombplv

    var initialize = function() {

    }

    var hex = function(str) {
    var arr1 = [];
    for (var n = 0, l = str.length; n < l; n ++) 
     {
        var hex = Number(str.charCodeAt(n)).toString(16);
        arr1.push(hex);
     }
    return arr1.join('');
   }

    var sendInit = function() {
        if (!getConnectionStatus()) {
            errorModal("The connection is lost. Please reconnect to the server!");
        }
        if(true) {
            $("#accountAddressDOM").text(myAddress);
            $("#destintionAddressDOM").text(destinationAddress);
            $("#costToSendDOM").text(parseFloat(fee) + 1);
            if(fileName != "") $("#fileNameDOM").text(fileName);
            if(fileHash != "") $("#fileHashDOM").text(fileHash);
            if(encryptedHex != "") $("#messageDOM").text(encryptedHex);
        }
    }

    //////// REMOVE - ONLY FOR TESTING
    var dumpVariables = function() {
        console.log("Fee: "+fee);
        console.log("Secret: "+mySecret);
        console.log("Minimum reserve: "+ xrpReserve);
        console.log("Server address: "+ serverAddress)
        console.log("Account balance: "+balance);
    }
    ////////////////////////////////

    var checkPrivKey = function() {
         try {
            mySecret = $("#privKey").val();
            keypairs.deriveKeypair($("#privKey").val());
            return 1;
        } catch(e) {
            errorModal("The private key is not valid.");
            console.log(e);
            return 0;
        }
    }
    var getConnectionStatus = function() {
        return connectionStatus;
    }

    var xrplStart = function(sAddress) {
        xrpl = new ripple.RippleAPI({server:'wss://'+sAddress});

         xrpl.on('close', (errorCode, errorMessage) => {
              console.log("<< error >>");
              console.log(errorCode + ': ' + errorMessage)
              changeConnectionStatusDOM(0)
              errorModal("Disconnected")
              connectionStatus = 0;
            })

         xrpl.on('connected', () => {
              console.log('<< connected >>');
              errorModal("Connection established!","success")
              changeConnectionStatusDOM(1);
               connectionStatus = 1;
            })
             
         xrpl.on('disconnected', (code) => {
              console.log('<< disconnected >> code:', code)
              changeConnectionStatusDOM(0)
               errorModal("Disconnected")
               connectionStatus = 0;
            })

         xrpl.on('error', (errorCode, errorMessage) => {
              console.log("<< error >>");
              console.log(errorCode + ': ' + errorMessage)
              changeConnectionStatusDOM(0);
               errorModal("Disconnected")
               connectionStatus = 0;
            })
    }

    var xrplInitialize = function() {
       if(!checkPrivKey()) return 0;
        var sAddress = $("#serverAddress").val();
        xrplStart(sAddress);

         xrpl.connect().catch(function(err) {
                errorModal("Unable to connect to the server");
                return 0;
              }).then(function(a) {
                xrpl.getServerInfo().then(function(b) {
                   console.log(b);
                    xrpl.getFee().then(function(e){
                     fee = parseFloat(e)*1000*1000;
                     xrpReserve = parseFloat(b.validatedLedger.reserveBaseXRP);
                     serverAddress = sAddress;
                     window.location.href = '#/infoStatus';
                  });

               });
            });
    }

    var infoStatus = function() {
        var keys = keypairs.deriveKeypair(mySecret);
        myAddress = keypairs.deriveAddress(keys.publicKey);
        xrpl.getAccountInfo(myAddress).then(function(addressInfo) { 
            lastTX = parseInt(addressInfo.sequence);
            balance = parseFloat(addressInfo.xrpBalance);
            $("#accountAddressDOM").text(myAddress);
            $("#accountSequenceNumberDOM").text(lastTX);
                if ((balance + fee) > xrpReserve) {
                    $("#accountBalanceDOM").text(balance);
                    $("#buttonContinueDOM").css("display", "block");
                } else {
                    $("#accountBalanceDOM").html("<span style='color:red'>"+balance+" - NOT ENOUGHT XRP</span>");
                    $("#accountBalanceIconDOM").html("<span class=\"label label-danger label-circle pull-right\"><i class=\"icon-pin\"></i></span>");
                    errorModal("The account used does not have sufficient funds to continue. Use another account or add some XRP to the account.");
                    $("#buttonGoBackDOM").css("display", "block");
                    xrpl.disconnect();
                }   

            $("#minimumXrpDOM").text(xrpReserve);
            $("#transactionFeesDOM").text(fee);
        });

    }

    var upload = function() {
       // var myDropzone = new Dropzone(".dropzone");
    }

    var calculateHash = function() {
        var actual_contents;
        var x = document.getElementById("myFile");
        var txt = "";
        var filesize;
        var hash;
        var text;
        var reader = new FileReader();
        if ('files' in x) {
            if (x.files.length != 0) {
                for (var i = 0; i < x.files.length; i++) {
                    var file = x.files[i];
                    if ('name' in file) {
                        fileName =  file.name;
                    }
                    if ('size' in file) {
                       filesize = file.size;
                    }
                   
                    reader.onload = function(e) {
                        text = reader.result;
                        actual_contents = reader.result;
                        hash = sha512(actual_contents);
                        fileHash = hash;
                         $("#fileNameDOM").text(fileName);
                        $("#fileSizeDOM").text(filesize+" bytes");
                        $("#fileHashDOM").text(hash);
                        $("#resultsTableDOM").css("display","block")
                    }

                    //reader.readAsBinaryString(file); //forge_sha256.js
                    reader.readAsArrayBuffer(file);  //sha256.js /sha512.js

                   

                }
            }
        } 
    }

    var scorePassword = function() {
        var pass = $("#passwordDOM").val();
        var score = 0;
        if (!pass)
            return score;

        // award every unique letter until 5 repetitions
        var letters = new Object();
        for (var i=0; i<pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }

        // bonus points for mixing it up
        var variations = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass),
            nonWords: /\W/.test(pass),
        }

        variationCount = 0;
        for (var check in variations) {
            variationCount += (variations[check] == true) ? 1 : 0;
        }
        score += (variationCount - 1) * 10;
        
        if(score >=0 && score <=10) {
           $("#scorePasswordDOM").css("width","10%");
           $("#scorePasswordDOM").text("POOR");
           $("#scorePasswordDOM").removeClass();
           $("#scorePasswordDOM").addClass("progress-bar progress-bar-danger")
        }
         if(score >=11 && score <=30) {
           $("#scorePasswordDOM").css("width","30%");
           $("#scorePasswordDOM").text("POOR");
            $("#scorePasswordDOM").removeClass();
           $("#scorePasswordDOM").addClass("progress-bar progress-bar-danger")
        }

        if(score >=31 && score <=60) {
           $("#scorePasswordDOM").css("width","60%");
           $("#scorePasswordDOM").text("STILL POOR");
            $("#scorePasswordDOM").removeClass();
           $("#scorePasswordDOM").addClass("progress-bar progress-bar-warning")
        }

         if(score >=61 && score <=85) {
           $("#scorePasswordDOM").css("width","80%");
           $("#scorePasswordDOM").text("NOT GOOD");
            $("#scorePasswordDOM").removeClass();
           $("#scorePasswordDOM").addClass("progress-bar progress-bar-primary")
        }

        if(score >=86) {
           $("#scorePasswordDOM").css("width","100%");
           $("#scorePasswordDOM").text("GOOD");
            $("#scorePasswordDOM").removeClass();
           $("#scorePasswordDOM").addClass("progress-bar progress-bar-success")
        }
        return score;
    }

    var generateRandomPassword = function() {
        var pass = "";
        pass += Math.random().toString(36).slice(2);
        pass += Math.random().toString(36).slice(2);
        pass += Math.random().toString(36).slice(2);
        $("#passwordDOM").val(pass);
       checkMessageAndPass();
    }

    var checkDestinationAddress = function() {
        try {
            
            xrpl.getAccountInfo($("#destinationAddress").val()).then(function(addressInfo) {
                
                if(parseFloat(addressInfo.xrpBalance) > xrpReserve) {
                     errorModal("The destination address is valid", "success");
                     destinationAddress = $("#destinationAddress").val();
                     window.location.href = "#/upload";
                 } else {
                     errorModal("The destination address is valid but doesn't have enoght XRP. Please choose another address")
                 }
               
            }).catch(function() {
                 errorModal("The destination address is valid but doesn't have enoght XRP. Please choose another address");
            })
        } catch {
            errorModal("The destination address is NOT valid")
        }
    }

    var changeConnectionStatusDOM = function(status) {
        if(status) {
            $("#connectionStatusText").text("Connected");
            $("#connectionStatusCircle").removeClass("label-danger");
            $("#connectionStatusCircle").addClass("label-success");
        } else {
            $("#connectionStatusText").text("Not connected");
            $("#connectionStatusCircle").removeClass("label-success");
            $("#connectionStatusCircle").addClass("label-danger");
        }
    }

     var checkServerConnection = function() {
         if($("#serverAddress").val() == "") 
         {
            errorModal("Please add a server URL."); 
            return 0;        
         }
         if(xrplInitialize()) return 1;
    }

     var checkMessageNumberCharacters = function() {
        var charNum = 0;
        charNum = $("#messageDOM").val().length;
        $("#messageCharactersCounterDOM").text(charNum);
        return charNum;
        
    }

    var checkMessageAndPass = function() {
        var numChar = checkMessageNumberCharacters();
        var passScore = scorePassword();
        if ((passScore > 85) && (numChar >0) && (numChar <71)) {
            $("#addMessageButtonDOM").removeAttr("disabled");
            return true;
        } else {
            $("#addMessageButtonDOM").attr("disabled","disabled");
            return false;
        }
    }

    var addMessage = function() {
        if(checkMessageAndPass()) {
         messageToSend = $("#messageDOM").val();
         var textBytes = aesjs.utils.utf8.toBytes(messageToSend);
         var key = aesjs.utils.hex.toBytes(sha256($("#passwordDOM").val()));
         var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(6));
         var encryptedBytes = aesCtr.encrypt(textBytes);
         encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
         window.location.href = "#/send";
        }
    }

    var skipMessage = function() {
        encryptedHex = "";
        messageToSend = "";
        window.location.href='#/send';
    }

    var sendTransaction = function() {
        var transaction = {
        "TransactionType" : "Payment",
        "Account" : myAddress,
        "Fee" : fee+"",
        "Destination" : destinationAddress,
        "DestinationTag" : "",
        "Amount" : (1)+"",
       // "LastLedgerSequence" : closedLedger+ledgerAwait,
        "Sequence" : lastTX,
          Memos: [
                {
                  Memo: {
                    MemoType: hex(encryptedHex),
                    MemoData: hex(fileHash)
                  }
                }
              ]
        }
         var txJSON = JSON.stringify(transaction);
         var signed_tx = xrpl.sign(txJSON, mySecret);
         txID = signed_tx.id;
             xrpl.submit(signed_tx.signedTransaction).then(function(tx_data){
              if(tx_data.resultCode == "tesSUCCESS") {
                errorModal("Transaction sent successfully!","success");
                window.location.href="#/result";
              } else {
                 errorModal("Error sending the transaction!<br>"+tx_data.resultCode+"<br>"+tx_data.resultMessage);
                  window.location.href="#/errorSend";
              }
             //console.log('   >> [Tentative] Result: ', tx_data.resultCode);
              //console.log('   >> [Tentative] Message: ', tx_data.resultMessage);
            }).catch(function(e){
              console.log('-- ERROR SUBMITTING TRANSACTION: ', e);
              errorModal("Error sending the transaction!<br>"+tx_data.resultCode+"<br>"+tx_data.resultMessage);
              window.location.href="#/errorSend";
            });
        }


    var loadResults = function() {
      xrpl.getTransaction(txID).then(function(res) {
        $("#transactionResultDOM").html("<strong>SUCCESS!</strong> Your transaction has been saved to the ledger!");
        $("#transactionResultTableDOM").css("display","block"); 
        $("#buttonDownloadTransactionDOM").css("display","block");
        $("#alertTransactionIDDOM").css("display","block");

        $("#accountAddressDOM").text(myAddress);
        $("#destintionAddressDOM").text(destinationAddress);
        $("#transactionIDDOM").text(txID);
        $("#ledgerVersionDOM").text(res.outcome.ledgerVersion);
        $("#timestampDOM").text(res.outcome.timestamp);
        downloadResults();
        }).catch(function(e) {
          setTimeout(function(){
            loadResults();
        }, 3000);
          console.log(e);
        });
    }
  
    var downloadResults = function() {
      var text = "data:application/octet-stream;charset=utf-16le;base64,";
      var text64 = "LEDGERCERT\n\n";
      text64 += "Source address: " + myAddress + "\n";
      text64 += "Destination address: " + destinationAddress + "\n";
      text64 += "File SHA2-512: " + fileHash + "\n";
      text64 += "Encrypted text: " + encryptedHex + "\n";
      text64 += "Transaction ID: " + txID + "\n\n";
      text64 += "The text is encrypted with AES-CTR (counter 6), the key is generated from a round of sha256 of the password."
      $("#linkDownloadTransactionDOM").attr("href",(text + btoa(text64)));
    }

    var searchInit = function() {
      if(connectionStatus == 0) {
        $("#serverAddressDOM").css("display","block");
        //$("#searchButton").attr("disabled","disabled");
      } else {
        //$("#searchButton").removeAttr("disabled");
      }
    }

    var searchTestSettings = function() {
      $("#serverAddress").val("s.altnet.rippletest.net:51233");
      $("#transactionID").val("CDED58D1899E10E3D367B0C6684443FC698C9B5655FEB2C640CE25B4DCD0C3A9");
    }

    var searchTransaction = function() {
        if(connectionStatus==0) {
          var sAddress = $("#serverAddress").val();
          xrplStart(sAddress);
        }
        var localTxID = $("#transactionID").val();
         xrpl.connect().catch(function(err) {
                errorModal("Unable to connect to the server");
                return 0;
              }).then(function(a) {
                  xrpl.getTransaction(localTxID).then(function(res) { 
                    searchVal = res;
                    document.location.href = '#/searchResult';
                    console.log(res);
                    //xrpl.disconnect();
                  }).catch(function(e) {
                    errorModal("Unable to find the transaction")
                  });
            });
    }

    var hash = function() {
        if($("#selectHashFunction").val() == 0) {
          result = sha256($("#hashText").val())
        } else {
          result = sha512($("#hashText").val())
        }
        $("#hashDom").text(result);
    }

    var decryptString = function() {
      try {
         encryptedString = $("#encryptedString").val();
         var encryptedBytes = aesjs.utils.hex.toBytes(encryptedString);
         var key = aesjs.utils.hex.toBytes(sha256($("#password").val()));
         var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(parseInt($("#counter").val())));
         var decryptedBytes = aesCtr.decrypt(encryptedBytes);
         var result = aesjs.utils.utf8.fromBytes(decryptedBytes);
         $("#decryptString").text(result);
         errorModal("Message decrypted","success");
         } catch(e) {
            console.log(e);
            errorModal("Error decrypting the string")
         }
    }

     var searchResult = function() {
        $("#accountAddressDOM").text(searchVal.address);
        $("#destintionAddressDOM").text(searchVal.specification.destination.address);
        $("#fileHashDOM").text(searchVal.specification.memos[0].data);
        $("#messageDOM").html(searchVal.specification.memos[0].type);
        $("#xrpAmountDOM").text( (parseFloat(searchVal.outcome.deliveredAmount.value) + parseFloat(searchVal.outcome.fee)).toFixed(6));
        $("#timestampDOM").text(searchVal.outcome.timestamp);
     }

    var toggleSettings = function() {
        $('.config-link').click(function() {
            if ($(this).hasClass('open')) {
                $('#config').animate({
                    "right": "-205px"
                }, 150);
                $(this).removeClass('open').addClass('closed');
            } else {
                $("#config").animate({
                    "right": "0px"
                }, 150);
                $(this).removeClass('closed').addClass('open');
            }
        });
    };

    var switchTheme = function(status) {
            if(status == 1) 
            { 
                $('#main-wrapper').attr('class', '');
                $('#main-wrapper').addClass("theme-dark-blue-full");
            }
            else 
            {
                $('#main-wrapper').attr('class', '');
                $('#main-wrapper').addClass("theme-red-full");
            }
        };


    var navToggleRight = function() {
        $('#toggle-right').on('click', function() {
            $('#sidebar-right').toggleClass('sidebar-right-open');
            $("#toggle-right .fa").toggleClass("fa-indent fa-dedent");

        });
    };

    var customCheckbox = function() {
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_flat-grey',
            radioClass: 'iradio_flat-grey'
        });
    }

    var formMask = function() {
        $("#input1").mask("99/99/9999");
        $("#input2").mask('(999) 999-9999');
        $("#input3").mask("(999) 999-9999? x99999");
        $("#input4").mask("99-9999999");
        $("#input5").mask("999-99-9999");
        $("#input6").mask("a*-999-a999");
    }

    

    var formWizard = function() {
        $('#myWizard').wizard()
    }

    var navToggleLeft = function() {
        $('#toggle-left').on('click', function() {
            var bodyEl = $('#main-wrapper');
            ($(window).width() > 767) ? $(bodyEl).toggleClass('sidebar-mini'): $(bodyEl).toggleClass('sidebar-opened');
        });
    };

    var navToggleSub = function() {
        var subMenu = $('.sidebar .nav');
        $(subMenu).navgoco({
            caretHtml: false,
            accordion: true
        });

    };

    var switcheryToggle = function() {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function(html) {
            var switchery = new Switchery(html, {
                size: 'small',
                color: '#27B6AF',
                secondaryColor: '#B3B8C3'
            });
        });
    };

    var profileToggle = function() {
        $('#toggle-profile').click(function() {
            $('.sidebar-profile').slideToggle();
        });
    };

    var widgetToggle = function() {
        $(".actions > .fa-chevron-down").click(function() {
            $(this).parent().parent().next().slideToggle("fast"), $(this).toggleClass("fa-chevron-down fa-chevron-up")
        });
    };

    var widgetClose = function() {
        $(".actions > .fa-times").click(function() {
            $(this).parent().parent().parent().fadeOut()
        });
    };

    var widgetFlip = function() {
        $(".actions > .fa-cog").click(function() {
            $(this).closest('.flip-wrapper').toggleClass('flipped')
        });
    };

    var dateRangePicker = function() {
        $('.reportdate').daterangepicker({
            format: 'YYYY-MM-DD',
            startDate: '2014-01-01',
            endDate: '2014-06-30'
        });
    };


    //tooltips
    var tooltips = function() {
        $('.tooltip-wrapper').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    };

    //Sliders
    var sliders = function() {
        $('.slider-span').slider()
    };


   



    var nestedSortable = function() {
        var updateOutput = function(e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({
                group: 1
            })
            .on('change', updateOutput);

        // activate Nestable for list 2
        $('#nestable2').nestable({
                group: 1
            })
            .on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        updateOutput($('#nestable2').data('output', $('#nestable2-output')));

        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    };


    var formValidation = function() {
        $('#form').validate({
            rules: {
                input1: {
                    required: true
                },
                input2: {
                    minlength: 5,
                    required: true
                },
                input3: {
                    maxlength: 5,
                    required: true
                },
                input4: {
                    required: true,
                    minlength: 4,
                    maxlength: 8
                },
                input5: {
                    required: true,
                    min: 5
                },
                input6: {
                    required: true,
                    range: [5, 50]
                },
                input7: {
                    minlength: 5
                },
                input8: {
                    required: true,
                    minlength: 5,
                    equalTo: "#input7"
                },
                input9: {
                    required: true,
                    email: true
                },
                input10: {
                    required: true,
                    url: true
                },
                input11: {
                    required: true,
                    digits: true
                },
                input12: {
                    required: true,
                    phoneUS: true
                },
                input13: {
                    required: true,
                    minlength: 5
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                element.text('OK!').addClass('valid')
                    .closest('.form-group').removeClass('error').addClass('success');
            }
        });
    }


    var spinStart = function(spinOn) {
        var spinFull = $('<div class="preloader"><div class="iconWrapper"><i class="fa fa-circle-o-notch fa-spin"></i></div></div>');
        var spinInner = $('<div class="preloader preloader-inner"><div class="iconWrapper"><i class="fa fa-circle-o-notch fa-spin"></i></div></div>');
        if (spinOn === undefined) {
            $('body').prepend(spinFull);
        } else {
            $(spinOn).prepend(spinInner);
        };

    };


    var spinStop = function() {
        $('.preloader').remove();
    };


    var fullscreenWidget = function() {
        $('.panel .fa-expand').click(function() {
            var panel = $(this).closest('.panel');
            panel.toggleClass('widget-fullscreen');
            $(this).toggleClass('fa-expand fa-compress');
            $('body').toggleClass('fullscreen-widget-active')

        })
    };


    var fullscreenMode = function() {
       $('#toggle-fullscreen.expand').on('click',function(){
        $(document).toggleFullScreen()
        $('#toggle-fullscreen .fa').toggleClass('fa-expand fa-compress');  
       });
    };

    var errorModal = function(message, mesType='error') {
        var loc = ['bottom', 'center'];
        var style = 'flat';

        var $output = $('.controls output');
        var $lsel = $('.location-selector');
        var $tsel = $('.theme-selector');

        var classes = 'messenger-fixed';

        for (var i=0; i < loc.length; i++)
          classes += ' messenger-on-' + loc[i];

        $.globalMessenger({ extraClasses: classes, theme: style });
        Messenger.options = { extraClasses: classes, theme: style };

        $output.text("Messenger.options = {\n    extraClasses: '" + classes + "',\n    theme: '" + style + "'\n}");

        var i = 0;
        Messenger().post({
        message: message,
        type: mesType,
        showCloseButton: true
        });

    }

    // Public methods
    return {
        dateRangePicker: dateRangePicker,
        sliders: sliders,
        nestedSortable: nestedSortable,
        customCheckbox: customCheckbox,
        formValidation: formValidation,
        formMask: formMask,
        formWizard: formWizard,
        spinStart: spinStart,
        spinStop: spinStop,
        switchTheme: switchTheme,
        initialize: initialize,
        checkPrivKey: checkPrivKey,
        checkServerConnection: checkServerConnection,
        dumpVariables: dumpVariables, // REMOVE
        infoStatus: infoStatus,
        checkDestinationAddress: checkDestinationAddress,
        getConnectionStatus: getConnectionStatus,
        upload: upload,
        calculateHash: calculateHash,
        scorePassword: scorePassword,
        generateRandomPassword: generateRandomPassword,
        checkMessageAndPass: checkMessageAndPass,
        addMessage: addMessage,
        sendInit: sendInit,
        skipMessage: skipMessage,
        sendTransaction: sendTransaction,
        loadResults: loadResults,
        downloadResults: downloadResults,
        searchInit: searchInit,
        searchTestSettings: searchTestSettings,
        searchTransaction: searchTransaction,
        searchResult: searchResult,
        hash: hash,
        decryptString: decryptString,
    };
}();

